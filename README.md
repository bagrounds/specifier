# [specifier](https://bagrounds.gitlab.io/specifier)

Specify constraints for JavaScript objects.

[![build-status](https://gitlab.com/bagrounds/specifier/badges/master/build.svg)](https://gitlab.com/bagrounds/specifier/commits/master)
[![coverage-report](https://gitlab.com/bagrounds/specifier/badges/master/coverage.svg)](https://gitlab.com/bagrounds/specifier/commits/master)
[![license](https://img.shields.io/npm/l/specifier.svg)](https://www.npmjs.com/package/specifier)
[![version](https://img.shields.io/npm/v/specifier.svg)](https://www.npmjs.com/package/specifier)
[![downloads](https://img.shields.io/npm/dt/specifier.svg)](https://www.npmjs.com/package/specifier)
[![downloads-monthly](https://img.shields.io/npm/dm/specifier.svg)](https://www.npmjs.com/package/specifier)
[![dependencies](https://david-dm.org/bagrounds/specifier/status.svg)](https://david-dm.org/bagrounds/specifier)

## [Test Coverage](https://bagrounds.gitlab.io/specifier/coverage/lcov-report/index.html)

## [API Docs](https://bagrounds.gitlab.io/specifier/docs/index.html)

## Dependencies

### Without Tests

![Dependencies](https://bagrounds.gitlab.io/specifier/img/dependencies.svg)

### With Tests

![Test Dependencies](https://bagrounds.gitlab.io/specifier/img/dependencies-test.svg)

